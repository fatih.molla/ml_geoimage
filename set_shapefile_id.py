import ogr
shp_file_path = 'data\\vector\\vector.shp'
shapef = ogr.Open(shp_file_path, 1)
layer = shapef.GetLayerByIndex(0)
for i in range(layer.GetFeatureCount()):
	feature = layer.GetFeature(i)
	properties = feature.items()
	feature.SetField('id', i)
	feature.SetField('ag_field', properties['ag_field'])
	layer.SetFeature(feature)
	feature.items()
shapef.Destroy()
shapef = layer = feature = None

import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from functools import lru_cache

import gdal, ogr, osr, numpy


class Colormap:
    cmap = colors.ListedColormap(['#000000','#ff0000','#bf0000', '#7f0000', '#ffff00', '#bfbf00', '#7f7f00', '#00ffff', '#00bfbf','#007f7f', '#00ff00', '#00bf00', '#007f00'])
    boundaries = [-1, -0.2, -0.1, 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1]
    norm = colors.BoundaryNorm(boundaries, cmap.N, clip=True)
    Ndvi = (cmap, norm)

class Band:
    COASTAL_AEROSOL = 1
    BLUE = 2
    GREEN = 3
    RED = 4
    NEAR_INFRARED = 5
    SHORT_WAVE_INFRARED_1 = 6
    SHORT_WAVE_INFRARED_2 = 7
    PANCHROMATIC = 8
    CIRRUS = 9
    THERMAL_INFRARED_1 = 10
    THERMAL_INFRARED_2 = 11

    BAND = (
        (COASTAL_AEROSOL, 'Coastal Aerosol'),
        (BLUE, 'Blue'),
        (GREEN, 'Green'),
        (RED, 'Red'),
        (NEAR_INFRARED, 'Near Infrared (NIR)'),
        (SHORT_WAVE_INFRARED_1, 'Short Wave Infrared (SWIR) 1'),
        (SHORT_WAVE_INFRARED_2, 'Short Wave Infrared (SWIR) 2'),
        (PANCHROMATIC, 'Panchromatic'),
        (CIRRUS, 'Cirrus'),
        (THERMAL_INFRARED_1, 'Thermal Infrared (TIRS) 1'),
        (THERMAL_INFRARED_2, 'Thermal Infrared (TIRS) 2'),
    )

    def get_name(band):
        return dict(Band.BAND)[band]

class LandsatImage:

    def __init__(self, folder):
        self.folder = folder
        self.image_name = os.path.basename(self.folder)
        self.images = {}
        self.datas = {}
        for index, key in enumerate(dict(Band.BAND).keys()):
            band_name = LandsatImage.get_band_name(self.folder, self.image_name, key)
            raster = gdal.Open(band_name)
            if index == 0:
                self.geo_transform = raster.GetGeoTransform()
                self.projection = raster.GetProjectionRef()
            self.images[key] = raster
            self.datas[key] = raster.ReadAsArray()
        
        red = self.datas[Band.RED]
        nir = self.datas[Band.NEAR_INFRARED]
        ndvi  = 1.0 * (nir - red) / (nir + red)
        self.ndvi = ndvi

    def get_band(self, band):
        return self.images[band]

    def from_wgs84_to_image_transform(self):
        sourceSR = osr.SpatialReference()
        sourceSR.ImportFromWkt(osr.SRS_WKT_WGS84)

        targetSR = osr.SpatialReference()
        targetSR.ImportFromWkt(self.projection)

        coordTrans = osr.CoordinateTransformation(sourceSR, targetSR)
        
        def transfer(polygon):
            left, step_x, _, top, _, step_y = self.geo_transform
            points = list(map(lambda i: coordTrans.TransformPoint(i[0], i[1]), polygon))
            points = list(map(lambda i: ((i[0] - left)/step_x, (i[1]-top)/step_y), points))
            return Polygon(points)
        return transfer
        # return coordTrans

    def get_values_in_polygon(self, band, polygon, empty=None):
        x_max, x_min, y_max, y_min = polygon.get_min_max()
        x_max = int(x_max)
        x_min = int(x_min)
        y_max = int(y_max)
        y_min = int(y_min)
    
        band_obj = self.get_band(band)
        rect = self.datas[band][y_min:y_max,x_min:x_max]
        
        height, width = rect.shape
        geometry = polygon.get_geometry()

        coords = np.full((height, width), empty)

        for x in range(width):
            for y in range(height):
                point = ogr.Geometry(ogr.wkbPoint)
                point.AddPoint(x + x_min, y + y_min)
                if geometry.Contains(point):
                    coords[y][x] =  int(rect[y][x])
                    
        return coords

    def get_statistics_in_polygon_by_band(self, band, polygon, avg=True, std=True):
        result = ()
        data = self.get_values_in_polygon(band, polygon)
        values = data[data != None]
        sum = np.sum(values)
        count = len(values)
        if count == 0:
            print('something')

        result = ()
        if avg:
            avg_value = np.round(sum/count, 2)
            result += (avg_value,)
        if std:
            std_value = np.round(np.std(values))
            result += (std_value,)

        return result

    def get_statistics_in_polygon(self, polygon, avg=True, std=True, bands=None):
        result = ()
        band_list = list(map(lambda x: x[0], Band.BAND))
        if bands != None and len(bands) != 0:
            band_list = list(filter(lambda x: x in bands, band_list))
        for band in band_list:
            statistics = self.get_statistics_in_polygon_by_band(band, polygon, avg=avg, std=std)
            result += statistics
        return result
    

    def get_ndvi(self, polygon, empty=None):
        x_max, x_min, y_max, y_min = polygon.get_min_max()
        x_max = int(x_max)
        x_min = int(x_min)
        y_max = int(y_max)
        y_min = int(y_min)
        rect = self.ndvi[y_min:y_max,x_min:x_max]
        
        # cmap, norm = Colormap.Ndvi
        # plt.imshow(rect, cmap=cmap, norm=norm)
        # plt.show()

        height, width = rect.shape
        geometry = polygon.get_geometry()

        coords = np.full((height, width), empty)

        for x in range(width):
            for y in range(height):
                point = ogr.Geometry(ogr.wkbPoint)
                point.AddPoint(x + x_min, y + y_min)
                if geometry.Contains(point):
                    coords[y][x] =  np.round(rect[y][x], 2)
                    
        return coords
    
    def get_ndvi_avg(self, polygon):
        data = self.get_ndvi(polygon)
        values = data[data != None]
        sum = np.sum(values)
        count = len(values)
        avg = np.round(sum/count, 2)
        return avg

    @staticmethod
    def get_band_name(folder, image_name, band):
        band_name = '{}\{}_B{}.tif'.format(folder, image_name, band)
        return band_name

class Polygon:

    def __init__(self, polygon):
        self.polygon = polygon
    
    def get_geometry(self):
        ring = ogr.Geometry(ogr.wkbLinearRing)
        for point in self.polygon:
            ring.AddPoint(point[0], point[1])
        
        poly = ogr.Geometry(ogr.wkbPolygon)
        poly.AddGeometry(ring)
        return poly

    def get_for_plot(self):
        x = [i[0] for i in self.polygon]
        y = [i[1] for i in self.polygon]
        return (x, y)

    def to_image_crs(self, projection_transform):
        return projection_transform(self.polygon)
        # left, step_x, _, top, _, step_y = geo_ref
        # points = list(map(lambda i: projection_transform.TransformPoint(i[0], i[1]),self.polygon))
        # points = list(map(lambda i: ((i[0] - left)/step_x, (i[1]-top)/step_y), points))
        # return Polygon(points)

    def get_envelope(self):
        x_max, x_min, y_max, y_min = self.get_min_max()
        points = [ (x_min, y_min), (x_min, y_max), (x_max, y_max), (x_max, y_min), (x_min, y_min), ]
        return Polygon(points)

    def get_min_max(self):
        x = list(map(lambda i: i[0], self.polygon))
        y = list(map(lambda i: i[1], self.polygon))
        x_max = max(x)
        x_min = min(x)
        y_max = max(y)
        y_min = min(y)
        return (x_max, x_min, y_max, y_min)

    def get_size(self):
        x_max, x_min, y_max, y_min = self.get_min_max()
        width = int(np.ceil(x_max - x_min + 1))
        height = int(np.ceil(y_max - y_min + 1))
        return (width, height)

    def __str__(self):
        return str(self.polygon)

class Shapefile:

    def __init__(self, shp_file):
        self.shapef = ogr.Open(shp_file)
        self.layer = self.shapef.GetLayerByIndex(0)
        self.feature_count = self.layer.GetFeatureCount()

    def get_geometry(self, index):
        feature = self.layer.GetFeature(index)
        geometry = feature.GetGeometryRef()
        geo = geometry.GetGeometryRef(0)
        points = geo.GetPoints()
        return Polygon(points)

    def get_properties(self, index):
        feature = self.layer.GetFeature(index)
        return feature.items()


def write_to_csv(data, file_path):
    text = ""
    for row in data:
        line = ";".join(str(x) for x in row)
        text += line + "\n"
        print(line)
        
    with open(file_path, 'w') as f:
        f.write(text)

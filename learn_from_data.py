import random 
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

from keras.models import Sequential
from keras.optimizers import SGD
from keras.layers.core import Dense, Activation, Dropout
from keras.layers import Conv1D, Flatten
from keras.utils.np_utils import to_categorical
from keras import metrics


np.set_printoptions(suppress=True, linewidth=np.inf)


classes = [True, False]
c = len(classes)
classes_onehot = list(map(lambda j: np.array([float(int(i == j)) for i in range(c)]), list(range(c))))
class_dict = dict(zip(classes, classes_onehot))

print(class_dict)

with open('dataset.csv', 'r') as f:
    lines = [x.strip() for x in f.readlines()]
    data = np.array([[float(y) for y in x.split(';')] for x in lines])

test_ratio = 0.2

x_data = data[:,:-1]
y_data = np.array([class_dict[x] for x in data[:,-1]]) # np.array([ class_dict[x<.6 and x>.4] for x in data[:,-1]])
data = list(zip(x_data, y_data))
random.shuffle(data)
test_count = int(test_ratio * len(data))
train = data[test_count:]
test = data[0:test_count]
x_train = np.array(list(map(lambda k: k[0], train)))
y_train = np.array(list(map(lambda k: k[1], train)))
x_test = np.array(list(map(lambda k: k[0], test)))
y_test = np.array(list(map(lambda k: k[1], test)))


h_input, w_input = x_train.shape
_, w_output = y_train.shape

model = Sequential()

model.add(Dense(128, input_shape=(w_input,), kernel_initializer="uniform"))
model.add(Activation('relu'))
model.add(Dropout(0.1))

model.add(Dense(2048, kernel_initializer="uniform"))
model.add(Activation('relu'))

model.add(Dense(2048, kernel_initializer="uniform"))
model.add(Activation('relu'))
model.add(Dropout(0.1))

model.add(Dense(128, kernel_initializer="uniform"))
model.add(Activation('relu'))
model.add(Dropout(0.1))

model.add(Dense(w_output, kernel_initializer="uniform"))
model.add(Dropout(0.1))
model.add(Activation('softmax'))

sgd = SGD(lr=0.01, decay=1e-6, momentum=0.5, nesterov=True)
model.compile(loss = 'categorical_crossentropy', optimizer = sgd, metrics=['acc']) # 
history = model.fit(x_train, y_train, epochs = 300, validation_split = 0.50, verbose = 1)


y_train_index = y_train.argmax(axis=1)
y_pred_train_index = model.predict_classes(x_train, verbose = 1)
correct = np.sum(y_pred_train_index ==  y_train_index)
print ('Train Accuracy: ', correct/float(y_train_index.shape[0])*100.0, '%')
# print(y_pred_train_index)
# print(y_train_index)



y_test_index = y_test.argmax(axis=1)
y_pred_index = model.predict_classes(x_test, verbose = 1)
correct = np.sum(y_pred_index ==  y_test_index)
print ('Test Accuracy: ', correct/float(y_test_index.shape[0])*100.0, '%')
# print(y_pred_index)
# print(y_test_index)
results = list(zip(y_test_index, y_pred_index))
true_positive = list(filter(lambda x: x[0] == True and x[0] == x[1], results))
true_negative = list(filter(lambda x: x[0] == False and x[0] == x[1], results))

false_positive = list(filter(lambda x: x[1] == True and x[0] != x[1], results))
false_negative = list(filter(lambda x: x[1] == False and x[0] != x[1], results))

print('TP: ', len(true_positive), 'TN: ', len(true_negative) )
print('FP: ', len(false_positive), 'FN: ', len(false_negative) )

print('TP: ', true_positive)
print('TN: ', true_negative)
print('FP: ', false_positive)
print('FN: ', false_negative)

# matrix = metrics.confusion_matrix(y_test_index, y_pred_index)


print(history.history.keys())

plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()


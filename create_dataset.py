import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors
from functools import lru_cache

import gdal, ogr, osr, numpy

from core import Colormap, Band, LandsatImage, Polygon, Shapefile, write_to_csv


cmap, norm = Colormap.Ndvi

raster_path = 'data\\LC08_L1TP_175034_20180607_20180615_01_T1'

landsat_image = LandsatImage(raster_path)
wgs84_to_image = landsat_image.from_wgs84_to_image_transform()

# red_band = landsat_image.get_band(Band.RED)
# red = red_band.ReadAsArray()
# plt.imshow(red, cmap='gray', vmin=0, vmax=2**16)

ndvi = landsat_image.ndvi
plt.imshow(ndvi, cmap=cmap, norm=norm)

shp_file_path = 'data\\vector\\vector.shp'
shp_file = Shapefile(shp_file_path)

result = []
for index in range(shp_file.feature_count):
    polygon = shp_file.get_geometry(index)
    properties = shp_file.get_properties(index)

    polygon_in_image = polygon.to_image_crs(wgs84_to_image)
    x, y = polygon_in_image.get_for_plot()
    plt.plot(x, y)

    # envelope = polygon.get_envelope()
    # envelope_in_image = envelope.to_image_crs(wgs84_to_image)
    # x, y = envelope_in_image.get_for_plot()
    # plt.plot(x, y)
    
    
    statistics=landsat_image.get_statistics_in_polygon(polygon_in_image, std=False)
    
    
    ndvi = landsat_image.get_ndvi(polygon_in_image, empty=0.0)
    # plt.imshow(ndvi, cmap=cmap, norm=norm)
    # plt.show()
    # print(ndvi)
    ndvi_avg = landsat_image.get_ndvi_avg(polygon_in_image)
    statistics = list(map(lambda x: x/(2**16), statistics))
    if properties['ag_field'] != None:
        data = list(statistics + [properties['ag_field']])
    else: 
        data = list(statistics + [1 if ndvi_avg > .4 and ndvi_avg < .6 else 0])
    result += [data]
    print(index, data)


write_to_csv(result, 'dataset.csv')

plt.show()




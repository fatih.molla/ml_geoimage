import random 
import numpy as np
import matplotlib.pyplot as plt
from functools import reduce

with open('dataset.csv', 'r') as f:
    lines = [x.strip() for x in f.readlines()]
    data = np.array([[float(y) for y in x.split(';')] for x in lines], dtype=np.float)

h,w = data.shape
feature_count = w - 1

distances = []
for i in range(h):
    for j in range(i+1, h):
        distance = np.sqrt(reduce(lambda a,b: a+b, map(lambda x: np.square(x[0] - x[1]), zip(data[i], data[j])), 0))
        distances.append(distance)

distances = np.array(distances)
max = np.ceil(distances.max())
min = np.floor(distances.min())
print(min, max)

counts, bins = np.histogram(distances, bins=np.arange(min, max, 0.001))
plt.hist(bins[:-1], bins, weights=counts)
plt.show()


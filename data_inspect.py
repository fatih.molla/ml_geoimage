import random 
import numpy as np
import matplotlib.pyplot as plt


with open('dataset.csv', 'r') as f:
    lines = [x.strip() for x in f.readlines()]
    data = np.array([[float(y) for y in x.split(';')] for x in lines], dtype=np.float)

h,w = data.shape
feature_count = w - 1

plt.figure(num=None, figsize=(14, 14), dpi=80, facecolor='w', edgecolor='k')
k = 0
for i in range(feature_count):
    for j in range(feature_count):
        k += 1
        ax = plt.subplot(feature_count, feature_count, k)
        if i == 0:
            ax.set_xlabel(j+1, verticalalignment='top')
            ax.xaxis.set_label_position('top') 

        if j == 0:
            ax.set_ylabel(i+1, rotation='horizontal', verticalalignment='center')

        ax.set_yticklabels([])
        ax.set_xticklabels([])
        
        if i != j:
            labels = np.array(list(map(lambda x: 1 if x > .4 and x <.6 else 0, data[:,-1])))
            dt = np.array(list(range(h)))
            x1 = list(filter(lambda x: x != None, np.where(labels, data[:, i], None)))
            y1 = list(filter(lambda x: x != None, np.where(labels, data[:, j], None)))
            x2 = list(filter(lambda x: x != None, np.where(labels != 1, data[:, i], None)))
            y2 = list(filter(lambda x: x != None, np.where(labels != 1, data[:, j], None)))

            ax.plot(x2, y2, 'bx')
            ax.plot(x1, y1, 'r.')

plt.show()




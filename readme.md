The aim of project is find agricultural fields by using neural network and landsat images. Project has two input, one of them is lansat image of interested area. The other is shape file which has labeled polygon data. By use this two file create a csv file for leaning input. As known landsat image has eleven bands. Each row of csv file contains this eleven bands average values which points was inside the polygon and label value of polygon.

```mermaid
graph LR;
    code1(create_dataset.py);
    code2(learn_from_data.py);
    intermediate_file[data.csv];
    %% style code1 fill:#f9f,stroke:#333,stroke-width:4px
    %% style code2 fill:#ccf,stroke:#f66,stroke-width:2px,stroke-dasharray: 5, 5
    style code1 fill:#f9f,stroke:#333,stroke-width:2px
    style code2 fill:#f9f,stroke:#333,stroke-width:2px
    
    shape_file --> code1;
    landsat_image --> code1;
    code1 --> intermediate_file;
    intermediate_file --> code2;
    code2 --> model;
```


### data.csv content

| COASTAL_AEROSOL | BLUE | GREEN | RED | NIR | SW_INFRA_1 | SW_INFRA_2 | PANCHROMATIC | CIRRUS | THERMAL_INFRARED_1  | THERMAL_INFRARED_2 | LABEL |
|-----------------|------|-------|-----|---------------|-----------------------|-----------------------|--------------|--------|---------------------|--------------------|-------|
| 0.155 | 0.138 | 0.126 | 0.109 | 0.365 | 0.174 | 0.114 | 0.154 | 0.0768 | 0.439 | 0.399 | 1 |
| 0.155 | 0.139 | 0.126 | 0.110 | 0.362 | 0.176 | 0.117 | 0.154 | 0.0768 | 0.439 | 0.399 | 1 |
| 0.155 | 0.138 | 0.126 | 0.109 | 0.356 | 0.169 | 0.113 | 0.149 | 0.0768 | 0.439 | 0.399 | 1 |


### Some conda commands 
```bash
conda env export > environment.yml
conda env create -n {name} -f environment.yml

conda info --envs  
conda create --name envname
conda remove --name envname --all
conda create --name clone_envname --clone envname
```